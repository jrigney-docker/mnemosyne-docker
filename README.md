# mnemosyne-docker

A docker image to run a mnemosyne sync server based on 2.4.1

http://mnemosyne-proj.org/

## todo

- create ansible to deploy mnemosyne

- create ansible to schedule backup on server

- create scripts to backup server

~~create a data volume container to contain backup directories~~

~~create a backup job for mnemosyne data~~

~~create a setup script for the data container and mnemosyne~~

## setup, run, & maintain it

### create the data containers
```
scripts/volumesCreated.sh
```

### copy the config and shared data to the data volume containers

ssh <host> mkdir ~/mnemosyne-docker/backup
rsync -avz ./backup <host>:~/mnemosyne-docker

rsync -mnemosyne-docker/backup
```
scripts/dirsRestored.sh
```

### check data volume containers have data
???


### run the container
```
docker run -d --name mnemosyne -p 8512:8512  -p 8513:8513  --volumes-from mnemosyne-share --volumes-from mnemosyne-config jrigney/mnemosyne:2.4.1
```

### firewall changes
???

### backing up the config and shared data from the data volume containers
```
scripts/dirsBackedup.sh
```

### debug the container

retrieve logs on the container

```
docker logs <container_name>
```

access a shell in the container on startup

```
docker run --rm --entrypoint bash --name mnemosyne -p 8512:8512  -p 8513:8513  --volumes-from mnemosyne-share --volumes-from mnemosyne-config -it mnemosyne
```

