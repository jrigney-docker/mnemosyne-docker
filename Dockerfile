FROM python:3.6

ADD Mnemosyne-2.6.tar.gz /
WORKDIR /Mnemosyne-2.6

# runtime dependencies
RUN apt-get update && apt-get install -y --no-install-recommends \
  python-matplotlib \
  texlive \
  dvipng \
  libgl1-mesa-glx \
  libnss3 \
  libXtst-dev \
  libasound2 \
  libegl1-mesa-dev \
&& rm -rf /var/lib/apt/lists/* \
&& pip3 install -U pip setuptools \
&& pip3 install pyqt5 \
&& pip3 install cheroot \
&& pip3 install webob \
&& pip3 install pillow \
&& python setup.py install

WORKDIR /root

EXPOSE 8512 8513

ENTRYPOINT ["mnemosyne", "--sync-server", "--web-server"]
