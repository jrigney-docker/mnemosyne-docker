#!/bin/bash

## backed up share and config from data volume containers to backup
docker cp mnemosyne-share:/root/.local/share/mnemosyne/. backup/mnemosyneShare/.
docker cp mnemosyne-config:/root/.config/mnemosyne/. backup/mnemosyneConfig/.


