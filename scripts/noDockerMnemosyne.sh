docker stop mnemosyne
docker rm mnemosyne
docker rm mnemosyne-config
docker rm mnemosyne-share
docker volume prune -f
